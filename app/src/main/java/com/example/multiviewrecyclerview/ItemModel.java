package com.example.multiviewrecyclerview;

public class ItemModel {
    private String title;
    private int type;

    public static final int ONE_TYPE = 1;
    public static final int TWO_TYPE = 2;


    public ItemModel(String title, int type) {
        this.title = title;
        this.type = type;
    }
    public ItemModel(){}
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
