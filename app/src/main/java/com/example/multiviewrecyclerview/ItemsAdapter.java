package com.example.multiviewrecyclerview;

import android.content.ClipData;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ItemModel> lists;
    private Context context;
    private MyItemClickListener mItemClickListener;

    public ItemsAdapter(List<ItemModel> lists, Context context) {
        this.lists = lists;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        ItemModel model = lists.get(position);
        if(model !=null)
            return model.getType();
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType){
                case ItemModel.ONE_TYPE:
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_one,parent,false);
                    return new MyOneViewHolder(view);
                case ItemModel.TWO_TYPE:
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_two,parent,false);
                    return new MyTwoViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ItemModel model = lists.get(position);
        switch (model.getType()){
            case ItemModel.ONE_TYPE:
                ((MyOneViewHolder)holder).txtTitleone.setText(model.getTitle());

                break;
            case ItemModel.TWO_TYPE:
                ((MyTwoViewHolder)holder).txtTitletwo.setText(model.getTitle());
                break;
        }
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }

    public void setOnItemCLickListenerMe(MyItemClickListener mItemClickListener){
        this.mItemClickListener = mItemClickListener;
    }

    public class MyOneViewHolder extends RecyclerView.ViewHolder{
        TextView txtTitleone;
        public MyOneViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTitleone  = itemView.findViewById(R.id.one);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    if(mItemClickListener != null){
                        if(pos != RecyclerView.NO_POSITION){
                            mItemClickListener.myItemClickListener(pos);
                        }
                    }
                }
            });
        }
    }

    public class MyTwoViewHolder extends RecyclerView.ViewHolder{
        TextView txtTitletwo;
        public MyTwoViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTitletwo  = itemView.findViewById(R.id.two);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    if(mItemClickListener != null){
                        if(pos != RecyclerView.NO_POSITION){
                            mItemClickListener.myItemClickListener(pos);
                        }
                    }
                }
            });
        }
    }
}
